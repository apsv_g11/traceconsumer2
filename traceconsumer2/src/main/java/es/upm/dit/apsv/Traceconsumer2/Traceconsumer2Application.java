package es.upm.dit.apsv.traceconsumer2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import org.springframework.core.env.Environment;

import org.springframework.web.client.HttpClientErrorException;

import org.springframework.web.client.RestTemplate;

import java.util.function.Consumer;

@SpringBootApplication
public class Traceconsumer2Application {
	public static final Logger log = LoggerFactory.getLogger(Traceconsumer1Application.class);
    @Autowired
    private  TraceRepository traceRepository;
	@Autowired
    private  Environment env;	
	public static void main(String[] args) {
		SpringApplication.run(Traceconsumer1Application.class, args);
	}

/* 	@Bean("consumer")
	public Consumer<Trace> checkTrace() {
			return t -> {
					t.setTraceId(t.getTruck() + t.getLastSeen());
					tr.save(t);
			};
	} */

	@Bean("consumer")
	public Consumer<Trace> checkTrace() {
			return t -> {
				t.setTraceId(t.getTruck() + t.getLastSeen());
				traceRepository.save(t);
				TransportationOrder result = TransportationOrderRepository.findById(t.getTruck())
				
				if (result != null && result.getTruck()!= null && !result.getTruck().equal('')){
					result.setLastDate(t.getLastSeen());
					result.setLastLat(t.getLastLat());
					result.setLastLong(t.getLastLong());
					if (result.distanceToDestination() < 10)
						result.setSt(1);
					TransportationOrderRepository.save(result);
					log.ifo("Order updated: " + result);
				}
			}
	}
}
